server {
  listen ${LISTEN_PORT};

  location /public {
    alias /vol/public;
  }

  location / {
    uwsgi_pass                  ${APP_HOST}:${APP_PORT};
    include                     /etc/nginx/uwsgi_param;
    client_max_body_size        10M;
  }
}
