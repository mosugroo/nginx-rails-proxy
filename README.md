# nginx rails api proxy server

nginx Rails App proxy server

## usage

### environment variables
* `LISTEN_PORT` - port to listen on (default: `80`)
* `APP_HOST` - Hostname of the app to send requests to (default: `app`)
* `APP_PORT` - port of the app to forward the requests to (default: `3000`)
